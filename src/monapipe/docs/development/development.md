# Developing MONAPipe

## Development environment

Set up a development environment as follows:

- Prerequisites: 

    Python>=3.9

- Set-up a virtual environment:

```sh
python3 -m venv env
source env/bin/activate
```

- Upgrade `pip`:

```sh
pip install --upgrade pip
```

- Install `monapipe` in editable mode alongside with dev-requirements. Run in top-level folder:

```sh
pip install -r requirements.dev.txt -e .
```

## Contributing

MONAPipe is developed and reviewed jointly. The following conventions and constraints are designed to create a stable code basis in a joint repository.

We differentiate between the role of `maintainer` and `developer` corresponding with the max role in the repository. Developers can edit code and implement features as described below, maintainers shall monitor the code quality and make reviews.

### Branch convention

 - The `main` branch is supposed to be for releases only (maintainers and developers aren't allowed to push directly). Maintainers can merge changes from the `develop` branch into `main`.
 - All changes in the code basis have to be done in the `develop` branch. Maintainers and developers can't push changes directly to `develop`. Contributions should be integrated as follows:
    - Contributors with developer status in the repo: Create an issue followed by a merge request. With the merge request a feature branch is created from `develop` (please verify that it is `develop`), implement the new feature and send the merge request to one of the maintainers.
    - Contributors without membership in the repo: Create a fork from `develop`, implement the new feature and send a merge request to one of the maintainers. Please make sure that the fork relation keeps intact, otherwise, we can't integrate the merge into the repo.

### Constraints

#### Commits

- Use [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/). We recommend using `commitizen` (included in dev-requirements) which is a helper with conventional commits. Instead of commiting via `git commit`, use `cz commit`(or simply `cz c`).

#### Style

- Code: [PEP 8](https://www.python.org/dev/peps/pep-0008/)
- Documentation: [Google style docstrings (Chapter 3.8 from Google styleguide)](https://google.github.io/styleguide/pyguide.html#s3.8-comments-and-docstrings)

#### Coding

- Objects that are not supposed to be used outside the current scope MUST be named starting with `_` (underscore): [PEP 316](https://www.python.org/dev/peps/pep-0316/#id12)

#### Formatting

Use `black`-formatter. We recommend configuration within the code editor, otherwise:

```sh
black .
```

#### Linting

Use `pylint`. We recommend configuration within the code editor, otherwise:

```sh
pylint monapipe tests
```

#### Typing

Use `mypy` for type checking. We recommend configuration within the code editor, otherwise:
```sh
mypy monapipe tests
```

### Pre-commit hooks

For your convenience, pre-commit hooks are configured to check against the constraints. Provided, you have installed the development requirements (see above), activate `pre-commit` via:

```sh
pre-commit install
```

Once activated commits only going to be accepted when fulfil the constraints.
