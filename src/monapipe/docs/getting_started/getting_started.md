# Getting started

## Installation

{%
   include-markdown "../../../../README.md"
   start="<!--readme-installation-start-->"
   end="<!--readme-installation-end-->"
%}


## Loading models and using MONAPipe

After the installation, load the MONAPipe language model via:

```python
import monapipe.model

nlp = monapipe.model.load()
```

You can also use the German model from spaCy. Its DependencyParser constructs trees with [TIGER](https://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/tiger/) dependencies. You can install and load the large German model (smaller German models can be found [here](https://spacy.io/models/de)) via:

```sh
python -m spacy download de_core_news_lg
```

```python
import spacy

nlp = spacy.load("de_core_news_lg")
```

After piping a text, you can access e.g. tokens, vector representations, POS tags, morphology, lemmas, dependency labels and named-entity types:

```python
doc = nlp("Your text here ...")

for sent in doc.sents:
    for token in sent:
        print(token.text, token.vector_norm, token.pos_, token.morph, token.lemma_,  token.dep_, token.ent_type_)
```

## Example Notebooks

For a quick tour try our Jupyter Notebooks that guide you through built-in components of spaCy and custom components of MONAPipe:

* [1. Notebook Basics](notebooks/tutorial/1_notebook_basics.ipynb)
* [2. Textual Phenomena](notebooks/tutorial/2_textual_phenomena.ipynb)
* [3. Narrative Structures](notebooks/tutorial/3_narrative_structures.ipynb)
* [4. Custom Component](notebooks/tutorial/4_custom_component.ipynb)

Additionally, we provide notebooks for specific components and their individidual implementations:

* [Coref](notebooks/components/coref.ipynb)
* [EntityLinker](notebooks/components/entity_linker.ipynb)
* [EntityRecognizer](notebooks/components/entity_recognizer.ipynb)
* [GenTagger](notebooks/components/gen_tagger.ipynb)
