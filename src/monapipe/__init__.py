# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

__version__ = "4.0.0"

# from monapipe.silence import (
#     silence_logging,
#     silence_tensorflow,
#     silence_tqdm,
#     silence_transformers,
#     silence_warnings,
# )

# silence_logging()
# silence_tensorflow()
# silence_tqdm()
# silence_transformers()
# silence_warnings()
