# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

from spacy.tokens import MorphAnalysis, Token

import monapipe.model
import monapipe.pipeline.clausizer.dependency_clausizer
import monapipe.pipeline.verb_analyzer.rb_verb_analyzer
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_rb_verb_analyzer():
    """Integration test for the `rb_verb_analyzer`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("dependency_clausizer")
    nlp.add_pipe("rb_verb_analyzer")
    doc = nlp(text_goethe_wv)
    assert check_data_types(doc._.clauses, "form", MorphAnalysis)
    assert check_data_types(doc._.clauses, "form_main", Token)
    assert check_data_types(doc._.clauses, "form_modals", list, Token)
    assert check_data_types(doc._.clauses, "form_verbs", list, Token)
