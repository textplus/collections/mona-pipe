# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
import monapipe.pipeline.formatter.conllu_formatter
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_conllu_formatter():
    """Integration test for the `conllu_formatter`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("conllu_formatter")
    doc = nlp(text_goethe_wv)
    assert check_data_types([doc], "format_str", str)
    assert check_data_types(doc.sents, "format_str", str)
