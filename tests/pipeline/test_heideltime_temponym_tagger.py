# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
import monapipe.pipeline.temponym_tagger.heideltime_temponym_tagger
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_heideltime_temponym_tagger():
    """Integration test for the `heideltime_temponym_tagger`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("heideltime_temponym_tagger")
    doc = nlp(text_goethe_wv)
    assert check_data_types(doc.spans["temponym"], "temponym_norm", dict, str, str)
