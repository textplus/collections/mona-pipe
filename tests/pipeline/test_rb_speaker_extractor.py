# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

from spacy.tokens import Span

import monapipe.model
import monapipe.pipeline.speaker_extractor.rb_speaker_extractor
import monapipe.pipeline.speech_tagger.quotation_marks_speech_tagger
from tests.methods import check_data_types
from tests.texts import text_goethe_wv, text_goethe_wv_full


def test_rb_speaker_extractor():
    """Integration test for the `rb_speaker_extractor`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("quotation_marks_speech_tagger")
    nlp.add_pipe("rb_speaker_extractor")
    doc = nlp(text_goethe_wv)
    assert check_data_types(doc.spans["speech"], "speaker", Span)
    if text_goethe_wv == text_goethe_wv_full:  # addressees only occur in `text_goethe_wv_full`
        assert check_data_types(doc.spans["speech"], "addressee", Span)
