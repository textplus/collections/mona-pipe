# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
from tests.texts import text_goethe_wv


def test_opentapioca():
    """Integration test for the external component `opentapioca`."""
    nlp = monapipe.model.load()
    nlp.add_pipe(
        "opentapioca", config={"url": "https://opentapioca.wordlift.io/api/annotate?lc=de"}
    )
    doc = nlp(text_goethe_wv)
