# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import random
import sys

# import TextGrid components
from docker.errors import BuildError
from tgclients.aggregator import Aggregator

import monapipe.model
import monapipe.pipeline.attribution_tagger.neural_attribution_tagger
import monapipe.pipeline.clausizer.dependency_clausizer
import monapipe.pipeline.event_tagger.neural_event_tagger
import monapipe.pipeline.gen_tagger.flair_gen_tagger
import monapipe.pipeline.gen_tagger.neural_gen_tagger
import monapipe.pipeline.ner.bert_character_ner
import monapipe.pipeline.reflection_tagger.neural_reflection_tagger
import monapipe.pipeline.speech_tagger.flair_speech_tagger

aggregator = Aggregator()

## Create pipeline object
nlp = monapipe.model.load(disable=["ner"])
## Add components
nlp.add_pipe("dependency_clausizer")

if len(sys.argv) > 1:
    pipe_names = sys.argv[1:]
else:
    pipe_names = [
        "bert_character_ner",
        random.choice(["flair_gen_tagger", "neural_gen_tagger"]),
        "flair_speech_tagger",
        "neural_attribution_tagger",
        "neural_event_tagger",
        "neural_reflection_tagger",
    ]

try:
    for pipe_name in pipe_names:
        nlp.add_pipe(pipe_name)
# for debugging docker issues
except BuildError as e:
    print("BuildError occurred:")
    print(e)
    for log in e.build_log:
        if "stream" in log:
            print(log["stream"].strip())
        if "errorDetail" in log:
            print(log["errorDetail"]["message"].strip())

print(nlp.pipe_names)

print("loading textgrid text...")
text_goethe_wahlverwandtschaften = aggregator.text("textgrid:11hnp.0").text
print("piping doc...")
doc_goethe_wahlverwandtschaften = nlp(text_goethe_wahlverwandtschaften[24399:33069])

# print named entities and their labels
if "bert_character_ner" in pipe_names:
    for ent in doc_goethe_wahlverwandtschaften.ents:
        print(ent, ent.label_)

# print speech spans
if "flair_speech_tagger" in pipe_names:
    for speech_span in doc_goethe_wahlverwandtschaften.spans["speech"]:
        print(speech_span)
        print(speech_span._.speech)

# print clauses and their attribution
if "neural_attribution_tagger" in pipe_names:
    for clause in doc_goethe_wahlverwandtschaften._.clauses:
        print(clause)
        print(clause._.attribution)

# print clauses and their event types
if "neural_event_tagger" in pipe_names:
    for clause in doc_goethe_wahlverwandtschaften._.clauses:
        print(clause)
        print(clause._.event["event_types"])

# print gi passages
if "neural_gen_tagger" in pipe_names:
    for gi_span in doc_goethe_wahlverwandtschaften.spans["gi"]:
        print(gi_span, gi_span._.gi)

# print reflective passages
if "neural_reflection_tagger" in pipe_names:
    for rp_span in doc_goethe_wahlverwandtschaften.spans["rp"]:
        print(rp_span, rp_span._.rp)
